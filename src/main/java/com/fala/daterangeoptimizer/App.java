package com.fala.daterangeoptimizer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fala.daterangeoptimizer.model.Order;
import com.fala.daterangeoptimizer.model.OrderDto;
import com.opencsv.bean.CsvToBeanBuilder;

import static java.util.stream.Collectors.toList;

public class App {

    public static void main(String[] args) throws FileNotFoundException {
        List<Order> orders = getOrders("/Users/sugamagarwal/Documents/workspace/optimize-routes/src/main/resources/hackathon21_workdata_v2.csv");

        System.out.println("Total Orders: " + orders.size());
        Map<LocalDate, List<Order>> dateWiseOrderMap = orders.stream().collect(Collectors.groupingBy(Order::getDate));

        dateWiseOrderMap.forEach((key, value) -> processForDate(key, value));
    }

    private static void processForDate(LocalDate date, List<Order> orders) {
        System.out.println("===========================");
        System.out.println(date + " : " + orders.size());
        LocalDate nextDay = date.plusDays(1);
        List<Order> singleDayOrders = orders.stream().filter(order -> order.getMethod().equals("All Day") && order.getDeliveryStartDate().isEqual(nextDay)).collect(toList());
        List<Order> dateRangeDayOrders = orders.stream().filter(order -> {
            return order.getMethod().equals("RF") && (order.getDeliveryStartDate().isEqual(nextDay) || order.getDeliveryStartDate().isBefore(nextDay))
                    && (order.getDeliveryEndDate().isEqual(nextDay) || order.getDeliveryEndDate().isAfter(nextDay));
        }).collect(toList());
        singleDayOrders.stream().collect(Collectors.groupingBy(order -> order.getDeliveryStartDate()))
                .forEach((localDate, orders1) -> System.out.println("Next Day: Single Day: " + localDate + " : " + orders1.size()));
        dateRangeDayOrders.stream().collect(Collectors.groupingBy(order -> order.getDeliveryStartDate() + "_" + order.getDeliveryEndDate()))
                .forEach((s, orders1) -> System.out.println("Next Day: DateRange: " + s + " : " + orders1.size()));

        List<Order> futureSingleDayOrders = orders.stream().filter(order -> order.getMethod().equals("All Day") && order.getDeliveryStartDate().isAfter(nextDay)).collect(toList());
        List<Order> futuredateRangeOrders = orders.stream().filter(order -> {
            return order.getMethod().equals("RF") && (order.getDeliveryStartDate().isAfter(nextDay));
        }).collect(toList());
        futureSingleDayOrders.stream().collect(Collectors.groupingBy(order -> order.getDeliveryStartDate()))
                .forEach((localDate, orders1) -> System.out.println("Future Day: Single Day: " + localDate + " : " + orders1.size()));
        futuredateRangeOrders.stream().collect(Collectors.groupingBy(order -> order.getDeliveryStartDate() + "_" + order.getDeliveryEndDate()))
                .forEach((s, orders1) -> System.out.println("Future Day: DateRange: " + s + " : " + orders1.size()));
        System.out.println("===========================");
    }

    private static List<Order> getOrders(String fileName) throws FileNotFoundException {
        LocalDate start = LocalDate.of(2021, 9, 1);
        LocalDate end = LocalDate.of(2021, 9, 23);

        List<OrderDto> orderDtos = new CsvToBeanBuilder(new FileReader(fileName))
                .withType(OrderDto.class)
                .withSkipLines(1)
                .build()
                .parse();
        return orderDtos.stream().map(Mapper::map).collect(toList());
    }

    private static List<Order> getOrdersByService(String serviceType, List<Order> orders) {
        return orders.stream().filter(order -> order.getMethod().equals(serviceType)).collect(toList());
    }
}
