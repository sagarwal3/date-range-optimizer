package com.fala.daterangeoptimizer.service;

import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;

import com.fala.daterangeoptimizer.model.Truck;
import com.fala.daterangeoptimizer.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DateRangeOptimizationServiceImpl implements DateRangeOptimizationService {

    private final DistributionService distributionService;
    private final OptimizationService optimizationService;
    private final OrderRepository orderRepository;

    @PostConstruct
    public void init() {
        optimize();
    }

    public void optimize() {
        LocalDate start = LocalDate.of(2021, 9, 2);
        for (int i = 0; i <= 2; i++) {
            distributeAndOptimizeOrders(start.plusDays(i));
        }
    }

    @Override
    public void distributeAndOptimizeOrders(LocalDate date) {
        distributionService.distributeOrders(date.minusDays(1));
        optimizationService.optimizeOrdersForADay(date);
    }


    @Override
    public List<Truck> getOptimizedOrders(LocalDate date) {
        return orderRepository.optimizedOrders.get(date);
    }

}
