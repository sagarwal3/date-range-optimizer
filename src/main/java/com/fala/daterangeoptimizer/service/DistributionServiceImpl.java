package com.fala.daterangeoptimizer.service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.fala.daterangeoptimizer.model.DateRange;
import com.fala.daterangeoptimizer.model.Order;
import com.fala.daterangeoptimizer.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import static com.fala.daterangeoptimizer.model.ServiceType.ALL_DAY;
import static com.fala.daterangeoptimizer.model.ServiceType.RF;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class DistributionServiceImpl implements DistributionService {

    private final OrderRepository orderRepository;

    @Override
    public void distributeOrders(LocalDate createdAt) {
        List<Order> newOrders = orderRepository.newOrders.get(createdAt);
        LocalDate nextDay = createdAt.plusDays(1);
        //to be distributed
        List<Order> nextDayOrders = new ArrayList<>();
        List<DateRange> toBeCleaned = new ArrayList<>();
        List<Order> nextDaySDOrders = newOrders.stream().filter(order -> ALL_DAY.getText().equals(order.getMethod()) && order.getDeliveryStartDate().isEqual(nextDay)).collect(toList());
        if (orderRepository.sdLeftOverOrders.containsKey(nextDay)) {
            nextDaySDOrders.addAll(orderRepository.sdLeftOverOrders.get(nextDay));
            orderRepository.sdLeftOverOrders.remove(nextDay);
        }
        List<Order> nextDayDROrders = newOrders.stream().filter(order -> !ALL_DAY.getText().equals(order.getMethod()) && order.getDeliveryStartDate().isEqual(nextDay)).collect(toList());
        for (Map.Entry<DateRange, List<Order>> e : orderRepository.drLeftOverOrders.entrySet()) {
            DateRange key = e.getKey();
            List<Order> orders1 = e.getValue();
            if (key.getDeliveryStart().isEqual(nextDay)) {
                nextDayDROrders.addAll(orders1);
                toBeCleaned.add(key);
            }
        }
        nextDayDROrders.sort(Comparator.comparing(Order::getDeliveryEndDate));
        //UpdateLeft Over
        toBeCleaned.forEach(dateRange -> orderRepository.drLeftOverOrders.remove(dateRange));
        updateLeftOver(newOrders, nextDaySDOrders, nextDayDROrders, createdAt);

        long nextDayOrderCount = nextDaySDOrders.size() + nextDayDROrders.size();
        Map<LocalDate, Long> toBeBalancedExpectedOrderMap = new HashMap<>();
        Map<LocalDate, Double> toBeBalancedProportionMap = new HashMap<>();

        long expectedOrderSum = 0;
        if (nextDayDROrders.size() > 0) {
            LocalDate lastDRDate = nextDayDROrders.get(nextDayDROrders.size() - 1).getDeliveryEndDate();
            for (LocalDate deliveryDate = nextDay.plusDays(1); deliveryDate.isEqual(lastDRDate) || deliveryDate.isBefore(lastDRDate); deliveryDate = deliveryDate.plusDays(1)) {
                Map<LocalDate, Integer> deliveryDateOrders = orderRepository.deliveryDateWiseOrderCount.get(deliveryDate);
                long orderCount = deliveryDateOrders == null ? 0 : deliveryDateOrders.values().stream().mapToInt(i -> i).sum();
                long expectedOrderCount = orderCount + perDayForecastedOrders(deliveryDateOrders, nextDay, deliveryDate) * getDaysBetween(nextDay, deliveryDate);
                if (nextDayOrderCount > expectedOrderCount) {
                    toBeBalancedExpectedOrderMap.put(deliveryDate, expectedOrderCount);
                    expectedOrderSum += expectedOrderCount;
                }
            }
            if (toBeBalancedExpectedOrderMap.size() > 0) {
                long avgOrders = (toBeBalancedExpectedOrderMap.values().stream().mapToLong(l -> l).sum() + nextDayOrderCount) / (toBeBalancedExpectedOrderMap.size() + 1);
                long excessOrderCount = Math.min((nextDayOrderCount - avgOrders), nextDayDROrders.size());
                List<Order> excessOrders = nextDayDROrders.subList(0, (int) excessOrderCount);
                nextDayDROrders = nextDayDROrders.stream().filter(order -> !excessOrders.contains(order)).collect(toList());
                List<Order> assignedExcessOrder = new ArrayList<>();
                for (Map.Entry<LocalDate, Long> entry : toBeBalancedExpectedOrderMap.entrySet()) {
                    double proportion = 1;
                    if (expectedOrderSum != 0) {
                        proportion = toBeBalancedExpectedOrderMap.size() == 1 ? entry.getValue() / expectedOrderSum : 1 - entry.getValue() / expectedOrderSum;
                    } else if (toBeBalancedExpectedOrderMap.size() > 0) {
                        proportion = 1 / toBeBalancedExpectedOrderMap.size();
                    }
                    toBeBalancedProportionMap.put(entry.getKey(), proportion);
                    Long orderstoBeAdded = (long) (proportion * excessOrderCount);
                    List<Order> collect = excessOrders.stream().filter(order -> (order.getDeliveryStartDate().isBefore(entry.getKey()) || order.getDeliveryEndDate().isEqual(entry.getKey()))
                                    && (order.getDeliveryEndDate().isAfter(entry.getKey()) || order.getDeliveryEndDate().isEqual(entry.getKey()))).limit(orderstoBeAdded)
                            .map(order -> order.toBuilder().deliveryDate(entry.getKey()).build()).collect(toList());
                    assignedExcessOrder.addAll(collect);
                }
                if (excessOrders.size() > assignedExcessOrder.size()) {
                    Set<String> assignedOrders = assignedExcessOrder.stream().map(Order::getOrderId).collect(Collectors.toSet());
                    excessOrders.removeAll(assignedOrders);
                    nextDayDROrders.addAll(excessOrders);
                }
                Map<LocalDate, List<Order>> futureOrders = assignedExcessOrder.stream().collect(groupingBy(Order::getDeliveryDate));
                futureOrders.entrySet().forEach(entry -> {
                    List<Order> futureDateOrders = orderRepository.distributedOrders.getOrDefault(entry.getKey(), new ArrayList<>());
                    futureDateOrders.addAll(entry.getValue());
                    orderRepository.distributedOrders.put(entry.getKey(), futureDateOrders);
                });
            }

        }
        nextDaySDOrders = nextDaySDOrders.stream().map(order -> order.toBuilder().deliveryDate(nextDay).build()).collect(toList());
        nextDayDROrders = nextDayDROrders.stream().map(order -> order.toBuilder().deliveryDate(nextDay).build()).collect(toList());
        nextDayOrders.addAll(nextDaySDOrders);
        nextDayOrders.addAll(nextDayDROrders);
        List<Order> nextdayExistingOrders = orderRepository.distributedOrders.getOrDefault(nextDay, new ArrayList<>());
        nextdayExistingOrders.addAll(nextDayOrders);
        orderRepository.distributedOrders.put(nextDay, nextdayExistingOrders);
        System.out.println("Assigned orders for date:" + nextDay + " and count " + nextdayExistingOrders.size());
    }

    private long perDayForecastedOrders(Map<LocalDate, Integer> deliveryDateOrders, LocalDate createdDate, LocalDate deliveryDate) {
        if (deliveryDateOrders == null) {
            return 0;
        }
        List<Map.Entry<LocalDate, Integer>> sorted = deliveryDateOrders.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).collect(toList());
        long sum = 0;
        long divider = 1;
        for (int i = 0; i < sorted.size(); i++) {
            sum += sorted.get(i).getValue() * (i + 1);
            divider += i;
        }
        return sum / divider;
    }

    private void updateLeftOver(List<Order> newRemainingOrders, List<Order> nextDaySDOrders, List<Order> nextDayDROrders, LocalDate createdAt) {
        List<Order> newLeftOverOrders = newRemainingOrders.stream().filter(order -> !nextDayDROrders.contains(order) && !nextDaySDOrders.contains(order)).collect(toList());
        Map<LocalDate, List<Order>> sdLeftOverOrders = newLeftOverOrders.stream().filter(order -> ALL_DAY.getText().equals(order.getMethod())).collect(groupingBy(Order::getDeliveryStartDate));
        Map<DateRange, List<Order>> drLeftOverOrders = newLeftOverOrders.stream().filter(order -> RF.getText().equals(order.getMethod())).collect(groupingBy(DateRange::from));
        updateSDLeftOverOrders(sdLeftOverOrders);
        updateDRLeftOverOrders(drLeftOverOrders);

        sdLeftOverOrders.forEach((deliveryDate, orders) -> {
            Map<LocalDate, Integer> aDefault = orderRepository.deliveryDateWiseOrderCount.getOrDefault(deliveryDate, new HashMap<>());
            int drOrderCount = drLeftOverOrders.entrySet().stream().filter(entry -> entry.getKey().getDeliveryStart().isEqual(deliveryDate)).mapToInt(entry -> entry.getValue().size()).sum();
            aDefault.put(createdAt, orders.size() + drOrderCount);
            orderRepository.deliveryDateWiseOrderCount.put(deliveryDate, aDefault);
        });
    }

    public void updateSDLeftOverOrders(Map<LocalDate, List<Order>> sdLeftOverOrders) {
        sdLeftOverOrders.forEach((date, orders) -> {
            List<Order> existingOrders = orderRepository.sdLeftOverOrders.getOrDefault(date, new ArrayList<>());
            existingOrders.addAll(orders);
            orderRepository.sdLeftOverOrders.put(date, existingOrders);
        });
    }

    public void updateDRLeftOverOrders(Map<DateRange, List<Order>> drLeftOverOrders) {
        drLeftOverOrders.forEach((date, orders) -> {
            List<Order> existingOrders = orderRepository.drLeftOverOrders.getOrDefault(date, new ArrayList<>());
            existingOrders.addAll(orders);
            orderRepository.drLeftOverOrders.put(date, existingOrders);
        });
    }

    public long getDaysBetween(LocalDate date1, LocalDate date2) {
        return Math.abs(ChronoUnit.DAYS.between(date1, date2));
    }
}
