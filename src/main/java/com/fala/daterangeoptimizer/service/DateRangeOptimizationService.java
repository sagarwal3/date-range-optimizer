package com.fala.daterangeoptimizer.service;

import java.time.LocalDate;
import java.util.List;

import com.fala.daterangeoptimizer.model.Truck;

public interface DateRangeOptimizationService {

    void distributeAndOptimizeOrders(LocalDate date);

    List<Truck> getOptimizedOrders(LocalDate date);
}
