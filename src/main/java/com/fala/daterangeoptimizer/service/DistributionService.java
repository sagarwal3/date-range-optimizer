package com.fala.daterangeoptimizer.service;

import java.time.LocalDate;

public interface DistributionService {

    void distributeOrders(LocalDate createdAt);
}
