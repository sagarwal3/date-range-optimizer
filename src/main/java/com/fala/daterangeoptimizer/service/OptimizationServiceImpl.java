package com.fala.daterangeoptimizer.service;

import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.fala.daterangeoptimizer.model.AssignedOrder;
import com.fala.daterangeoptimizer.model.DistanceMatrixes;
import com.fala.daterangeoptimizer.model.Location;
import com.fala.daterangeoptimizer.model.Order;
import com.fala.daterangeoptimizer.model.Truck;
import com.fala.daterangeoptimizer.repository.OrderRepository;
import com.google.common.collect.Lists;
import com.google.ortools.constraintsolver.Assignment;
import com.google.ortools.constraintsolver.FirstSolutionStrategy;
import com.google.ortools.constraintsolver.RoutingIndexManager;
import com.google.ortools.constraintsolver.RoutingModel;
import com.google.ortools.constraintsolver.RoutingSearchParameters;
import com.google.ortools.constraintsolver.main;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import static com.fala.daterangeoptimizer.model.Location.from;
import static com.fala.daterangeoptimizer.utility.OptimizerUtility.latitudeToY;
import static com.fala.daterangeoptimizer.utility.OptimizerUtility.longitudeToX;

@Service
@Slf4j
public class OptimizationServiceImpl implements OptimizationService {
    private final OrderRepository orderRepository;
    private final int truckCapacity = 100;
    private final DecimalFormat decimalFormatter;

    public OptimizationServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
        decimalFormatter = new DecimalFormat("0.000");
    }


    @Override
    public void optimizeOrdersForADay(LocalDate date) {
        List<Order> orders = orderRepository.distributedOrders.get(date);
        if (CollectionUtils.isNotEmpty(orders)) {
            List<Truck> trucks = optimizeTheRouteForADay(orders, truckCapacity);
            orderRepository.optimizedOrders.put(date, trucks);
            log.info("Delivery Date: {}, Orders Count: {}, Trucks Count: {}", date, orders.size(), trucks.size());
        }
    }

    // Calculate distance matrix using Euclidean distance.
    private DistanceMatrixes computeEuclideanDistanceMatrix(double[][] locations) {
        long[][] distanceMatrix = new long[locations.length][locations.length];
        String[][] doubleDistanceMatrix = new String[locations.length][locations.length];
        for (int fromNode = 0; fromNode < locations.length; ++fromNode) {
            for (int toNode = 0; toNode < locations.length; ++toNode) {
                if (fromNode == toNode) {
                    distanceMatrix[fromNode][toNode] = 0;
                } else {
                    double distance = Math.hypot(locations[toNode][0] - locations[fromNode][0],
                            locations[toNode][1] - locations[fromNode][1]);
                    doubleDistanceMatrix[fromNode][toNode] = decimalFormatter.format(distance);
                    distanceMatrix[fromNode][toNode] = (long) distance;
                }
            }
        }
        return DistanceMatrixes.builder().doubleDistanceMatrix(doubleDistanceMatrix).distanceMatrix(distanceMatrix).build();
    }

    private List<Truck> optimizeTheRouteForADay(List<Order> orders, int truckCapacity) {
        double[][] coordinates = prepareCoordinatesFromOrders(orders);
        final DistanceMatrixes distanceMatrixes = computeEuclideanDistanceMatrix(coordinates);
        final long[][] distanceMatrix = distanceMatrixes.getDistanceMatrix();
        RoutingIndexManager manager = new RoutingIndexManager(orders.size(), 1, 0);
        RoutingModel routing = new RoutingModel(manager);
        Assignment optimizedRoute = optimizeRoutes(manager, routing, distanceMatrix);
        List<Integer> route = getRoute(routing, manager, optimizedRoute);
        return prepareTruckOrders(route, orders, distanceMatrixes.getDoubleDistanceMatrix(), truckCapacity);
    }


    private List<Integer> getRoute(RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
        long index = routing.start(0);
        List<Integer> routes = new ArrayList<>();
        while (!routing.isEnd(index)) {
            routes.add(manager.indexToNode(index));
            long previousIndex = index;
            index = solution.value(routing.nextVar(index));
        }
        routes.add(manager.indexToNode(routing.end(0)));
        return routes;
    }

    private List<Truck> prepareTruckOrders(List<Integer> routes, List<Order> orders, String[][] doubleDistanceMatrix, int truckCapacity) {
        List<Truck> trucks = new ArrayList<>();
        List<List<Integer>> partitions = Lists.partition(routes, truckCapacity);
        int truckSequence = 1;
        for (List<Integer> indexes : partitions) {
            List<AssignedOrder> truckOrders = new ArrayList<>();
            double totalDistanceCovered = 0;
            int prevIndex = 0;
            int sequence = 0;
            for (int currentIndex : indexes) {
                Order currentOrder = orders.get(currentIndex);
                String distanceCovered = "0";
                if (currentIndex != 0) {
                    distanceCovered = doubleDistanceMatrix[prevIndex][currentIndex];
                    totalDistanceCovered += Double.parseDouble(distanceCovered);
                }
                truckOrders.add(AssignedOrder.builder()
                        .orderId(currentOrder.getOrderId())
                        .latitude(currentOrder.getLatitude())
                        .longitude(currentOrder.getLongitude())
                        .sequence(sequence++)
                        .distanceCovered(distanceCovered)
                        .build());

                prevIndex = currentIndex;
            }
            trucks.add(Truck.builder().truckId(truckSequence++).orders(truckOrders).averageDistance(totalDistanceCovered / truckOrders.size()).build());
        }
        return trucks;
    }

    private Assignment optimizeRoutes(RoutingIndexManager manager, RoutingModel routing, long[][] distanceMatrix) {
        final int transitCallbackIndex =
                routing.registerTransitCallback((long fromIndex, long toIndex) -> {
                    int fromNode = manager.indexToNode(fromIndex);
                    int toNode = manager.indexToNode(toIndex);
                    return distanceMatrix[fromNode][toNode];
                });
        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);
        RoutingSearchParameters searchParameters =
                main.defaultRoutingSearchParameters()
                        .toBuilder()
                        .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                        .build();
        return routing.solveWithParameters(searchParameters);
    }

    public double[][] prepareCoordinatesFromOrders(List<Order> orders) {
        List<Location> locations = new ArrayList<>();
        orders.forEach(order -> locations.add(from(order)));
        double[][] coordinatesArray = new double[locations.size()][2];
        int i = 0;
        for (Location coordinate : locations) {
            double x = longitudeToX(coordinate.getLongitude());
            double y = latitudeToY(coordinate.getLatitude());
            coordinatesArray[i][0] = x;
            coordinatesArray[i][1] = y;
            i++;
        }
        return coordinatesArray;
    }
}
