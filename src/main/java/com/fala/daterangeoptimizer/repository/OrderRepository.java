package com.fala.daterangeoptimizer.repository;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import com.fala.daterangeoptimizer.model.DateRange;
import com.fala.daterangeoptimizer.Mapper;
import com.fala.daterangeoptimizer.model.Order;
import com.fala.daterangeoptimizer.model.OrderDto;
import com.fala.daterangeoptimizer.model.ServiceType;
import com.fala.daterangeoptimizer.model.Truck;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.stereotype.Repository;

import static java.util.stream.Collectors.toList;

@Repository
public class OrderRepository {

    public List<Order> orders = new ArrayList<>();

    public Map<LocalDate, List<Order>> newOrders = new HashMap<>();

    public Map<LocalDate, List<Order>> sdLeftOverOrders = new HashMap<>();

    public Map<DateRange, List<Order>> drLeftOverOrders = new HashMap<>();

    public Map<LocalDate, Map<LocalDate, Integer>> deliveryDateWiseOrderCount = new HashMap<>();

    public Map<LocalDate, List<Order>> distributedOrders = new HashMap<>();

    public Map<LocalDate, List<Truck>> optimizedOrders = new HashMap<>();

    @PostConstruct
    public void init() throws FileNotFoundException {
        List<OrderDto> orderDtos = new CsvToBeanBuilder(new FileReader("/Users/sugamagarwal/Downloads/date-range-optimizer/src/main/resources/hackathon21_workdata_v2.csv"))
                .withType(OrderDto.class)
                .withSkipLines(1)
                .build()
                .parse();
        orders = orderDtos.stream().map(Mapper::map).collect(toList());
        newOrders = orders.stream().collect(Collectors.groupingBy(Order::getDate));
    }

}
