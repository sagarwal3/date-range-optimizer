package com.fala.daterangeoptimizer.utility;

public class OptimizerUtility {

    public static double latitudeToY(double latitude) {
        return ((1 - Math.log(Math.tan(latitude * Math.PI / 180) + 1 / Math.cos(latitude * Math.PI / 180)) / Math.PI) / 2 * Math.pow(2, 0)) * 256;
    }

    public static double longitudeToX(double lontitudes) {
        return (lontitudes + 180f) / 360f * 256f;
    }
}
