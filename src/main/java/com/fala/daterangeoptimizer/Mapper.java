package com.fala.daterangeoptimizer;

import java.time.LocalDate;

import com.fala.daterangeoptimizer.model.Order;
import com.fala.daterangeoptimizer.model.OrderDto;

public class Mapper {

    public  static Order map(OrderDto orderDto) {
        return Order.builder().orderId(orderDto.getOrderId())
                .createdAt(orderDto.getCreatedAt())
                .deliveryStartDate(LocalDate.parse(orderDto.getDeliveryStartDate()))
                .deliveryEndDate(LocalDate.parse(orderDto.getDeliveryEndDate()))
                .latitude(orderDto.getLatitude())
                .longitude(orderDto.getLongitude())
                .method(orderDto.getMethod())
                .date(LocalDate.parse(orderDto.getDate()))
                .build();
    }
}
