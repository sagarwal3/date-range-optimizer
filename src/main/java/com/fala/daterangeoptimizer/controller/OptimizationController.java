package com.fala.daterangeoptimizer.controller;

import java.time.LocalDate;
import java.util.List;

import com.fala.daterangeoptimizer.model.Truck;
import com.fala.daterangeoptimizer.service.DateRangeOptimizationService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class OptimizationController {

    private final DateRangeOptimizationService dateRangeOptimizationService;


    @GetMapping("/order/trucks")
    public ResponseEntity<List<Truck>> getOrdersForTheDate(@RequestParam("orderDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate date) {
        return ResponseEntity.ok(dateRangeOptimizationService.getOptimizedOrders(date));
    }
}
