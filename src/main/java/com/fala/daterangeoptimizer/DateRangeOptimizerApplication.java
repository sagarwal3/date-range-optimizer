package com.fala.daterangeoptimizer;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.ortools.Loader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DateRangeOptimizerApplication {

	public static void main(String[] args) {
		Loader.loadNativeLibraries();
		SpringApplication.run(DateRangeOptimizerApplication.class, args);
	}

}
