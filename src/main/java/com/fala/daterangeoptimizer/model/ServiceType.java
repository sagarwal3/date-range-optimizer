package com.fala.daterangeoptimizer.model;

public enum ServiceType {
    ALL_DAY("All Day"),
    RF("RF");

    public String getText() {
        return text;
    }

    String text;

    ServiceType(String s) {
        this.text = s;
    }


}
