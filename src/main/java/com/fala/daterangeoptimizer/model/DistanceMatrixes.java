package com.fala.daterangeoptimizer.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class DistanceMatrixes {
    long[][] distanceMatrix;
    String[][] doubleDistanceMatrix;
}
