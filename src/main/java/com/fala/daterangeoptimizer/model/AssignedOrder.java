package com.fala.daterangeoptimizer.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class AssignedOrder {
    int sequence;
    String orderId;
    String latitude;
    String longitude;
    String distanceCovered;

    public static AssignedOrder from(Order order, int sequence) {
        return AssignedOrder.builder()
                .orderId(order.getOrderId())
                .sequence(sequence)
                .latitude(order.getLatitude())
                .longitude(order.getLongitude())
                .build();
    }
}
