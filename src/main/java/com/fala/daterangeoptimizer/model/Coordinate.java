package com.fala.daterangeoptimizer.model;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Coordinate {
    private String orderId;
    private double x;
    private double y;
    private double latitude;
    private double longitude;
}
