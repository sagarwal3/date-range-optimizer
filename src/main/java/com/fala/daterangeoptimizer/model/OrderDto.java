package com.fala.daterangeoptimizer.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

@Data
public class OrderDto {
    @CsvBindByPosition(position = 1)
    String orderId;
    @CsvBindByPosition(position = 2)
    String createdAt;
    @CsvBindByPosition(position = 3)
    String deliveryStartDate;
    @CsvBindByPosition(position = 4)
    String deliveryEndDate;
    @CsvBindByPosition(position = 5)
    String latitude;
    @CsvBindByPosition(position = 6)
    String longitude;
    @CsvBindByPosition(position = 7)
    boolean stContains;
    @CsvBindByPosition(position = 8)
    String method;
    @CsvBindByPosition(position = 9)
    String quantity;
    @CsvBindByPosition(position = 10)
    String weight;
    @CsvBindByPosition(position = 11)
    String volume;
    @CsvBindByPosition(position = 12)
    String date;
}
