package com.fala.daterangeoptimizer.model;

import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class Truck {
    int truckId;
    List<AssignedOrder> orders;
    Double averageDistance;
}
