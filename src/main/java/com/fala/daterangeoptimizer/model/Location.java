package com.fala.daterangeoptimizer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class Location {
    String orderId;
    Double latitude;
    Double longitude;

    @JsonIgnore
    public static Location from(Order order) {
       return Location.builder().
                latitude(Double.valueOf(order.getLatitude()))
                .longitude(Double.valueOf(order.getLongitude()))
                .orderId(order.getOrderId())
                .build();
    }
}
