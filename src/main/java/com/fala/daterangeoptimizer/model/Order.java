package com.fala.daterangeoptimizer.model;

import java.time.LocalDate;

import lombok.Builder;
import lombok.Data;

@Builder(toBuilder = true)
@Data
public class Order {
    String orderId;
    String createdAt;
    LocalDate deliveryStartDate;
    LocalDate deliveryEndDate;
    String latitude;
    String longitude;
    String method;
    String quantity;
    String weight;
    String volume;
    LocalDate date;
    LocalDate deliveryDate;
}
