package com.fala.daterangeoptimizer.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class DateRange {
    LocalDate deliveryStart;
    LocalDate deliveryEnd;

    @JsonIgnore
    public static DateRange from(Order order) {
       return DateRange.builder()
                .deliveryStart(order.getDeliveryStartDate())
                .deliveryEnd(order.getDeliveryEndDate())
                .build();
    }
}
